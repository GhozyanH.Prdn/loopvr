<?php
class Dashboard extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model ('Model_event');
    if($this->session->userdata('masuk') != TRUE){
    $url=base_url();
    redirect($url);
  }
	}
  public function index()
	{
		if($this->session->userdata('akses')=='3'){
			$data['event'] = $this->Model_event->get();
      $this->load->view('peserta/dashboard',$data);
    }else{
     $this->load->view('login');
    }
}
public function aksi_upload(){
	$config['upload_path']          = './assets/images/bukti/';
	$config['allowed_types']        = 'gif|jpg|png|jpeg';
	$config['max_size']             = 100000;
	$config['max_width']            = 50000;
	$config['max_height']           = 50000;

	$this->load->library('upload', $config);

	if ( ! $this->upload->do_upload())
	{
		$this->load->view('V_error');

	}
	else{
		$img = $this->upload->data();
		$photo = $img['file_name'];
		$akun = $this->input->post('akun');
		$km = $this->input->post('km');
		$jam = $this->input->post('jam');
		$menit = $this->input->post('menit');
		$event = $this->input->post('event');
		$point = $this->input->post('point');
		$race = $this->input->post('race');
		$tgl_input = $this->input->post('tgl_input');

		$data = array(
			'akun' => $akun,
			'km' => $km,
			'jam' => $jam,
			'menit' => $menit,
			'event' => $event,
			'photo' => $photo,
			'point' => $point,
			'race' => $race,
			'tgl_input' => $tgl_input,
		);
		$this->db->insert('tbl_point',$data);
		$this->load->view('peserta/person_view');
		redirect('peserta/person');
	}
}
}
